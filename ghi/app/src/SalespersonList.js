import React, { useEffect, useState } from 'react';
import SalespersonCard from './SalespersonCard';


function SalespersonList() {
  const [salespersonCards, setSalespersonCards] = useState([]);

  const deleteSalesperson = async (index, id) => {
    const salespersonURL = 'http://localhost:8090/api/salespeople/' + id;
    const fetchConfig = {
      method: "delete",
    };

    const response = await fetch(salespersonURL, fetchConfig);
    if (response.ok) {
      const updatedSalespersonCards = salespersonCards.filter((_, i) => i !== index);
      setSalespersonCards(updatedSalespersonCards);
    }
  };

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespersonCards(data.salespeople);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container mt-4">
      <h2>A list of sales people</h2>
      <div className="row row-cols-3">
        {salespersonCards.map((salespersonCard, index) => {
          return (
            <SalespersonCard
              key={index}
              index={index}
              salesperson={salespersonCard}
              deletesalesperson={() => deleteSalesperson(index, salespersonCard.employee_id)}
            />
          );
        })}
      </div>
    </div>

  );
}
export default SalespersonList;
