import React from 'react';


function CustomerCard(props) {
  if (props.customer === undefined) {
    return null;
  }
  const cardStyle = {
    backgroundColor: '#198754',
    padding: '10px',
    borderRadius: '8px',
    color: 'white'
  };
  return (
    <div className="col">
      <div key={props.customer.first_name} className="card mb-3 shadow">
        <div className="card-body" style={cardStyle}>
          <h6 className="card-title">First name: {props.customer.first_name}</h6>
          <h6 className="card-subtitle mb-2">
            Last name: {props.customer.last_name}
          </h6>
          <h6 className="card-subtitle mb-2">
            Phone: {props.customer.phone_number}
          </h6>
          <h6 className="card-subtitle mb-2">
            Address: {props.customer.address}
          </h6>
        </div>
      </div>
    </div>
  );
}

export default CustomerCard;
