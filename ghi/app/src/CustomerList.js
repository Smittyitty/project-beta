import React, { useEffect, useState } from 'react';
import CustomerCard from './CustomerCard';


function CustomerList() {
  const [customerCards, setCustomerCards] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/customers/';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomerCards(data.customers);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container mt-4">
      <h2>A list of Customers</h2>
      <div className="row row-cols-3">
        {customerCards.map((customerCard, index) => {
          return (
            <CustomerCard
              key={index}
              index={index}
              customer={customerCard}
            />
          );
        })}
      </div>
    </div>
  )
}
export default CustomerList;
