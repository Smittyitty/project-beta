import React from 'react';


function SalespersonCard(props) {
  if (props.salesperson === undefined) {
    return null;
  }
  const cardStyle = {
    backgroundColor: '#198754',
    padding: '10px',
    borderRadius: '8px',
    color: 'white'
  };
  return (
    <div className="col">
      <div key={props.salesperson.first_name} className="card mb-3 shadow" style={cardStyle}>
        <div className="card-body" style={{color: 'white'}}>
          <h6 className="card-subtitle mb-2">First name: {props.salesperson.first_name}</h6>
          <h6 className="card-subtitle mb-2" style={cardStyle}>
            Last name: {props.salesperson.last_name}
          </h6>
          <h6 className="card-subtitle mb-2" style={cardStyle}>
            Employee ID: {props.salesperson.employee_id}
          </h6>
        </div>
      </div>
    </div>
  );
}

export default SalespersonCard;
