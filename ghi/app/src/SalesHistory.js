import React, { useEffect, useState } from 'react';

function SalesHistoryList() {
    const [sales, setSales] = useState([]);
    const [salespeep, setSalespeep] = useState([[]]);
    const [selectperson, setSelectperson] = useState('')

    async function loadSales() {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
        }
    }

    async function loadSalespeep() {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalespeep(data.salespeople);
            }
        }

    useEffect(() => {
    loadSales();
    loadSalespeep();
    }, []);

    const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSelectperson(value);
    }

    return (
        <div className="container my-4">
          <h1 className="display-5 fw-bold">Sales History by Employee</h1>
            <div className="my-4">
              <select
                className="form-select"
                onChange={handleSalespersonChange}
                value={selectperson}
                required
              >
                <option value="">Choose a Salesperson</option>
                {salespeep.map((salesperson) => (
                  <option value={salesperson.employee_id} key={salesperson.employee_id}>
                    {`${salesperson.first_name} ${salesperson.last_name} (${salesperson.employee_id})`}
                  </option>
                ))}
        </select>
      </div>
      {selectperson && (
        <table className="table table-striped">
          <thead>
              <tr>
                <th>Salesperson</th>
                <th>Customer</th>
                <th>Vin</th>
                <th>Price</th>
              </tr>
          </thead>
          <tbody>
            {sales.filter((sale) => sale.salesperson.employee_id === String(selectperson))
            .map((sale, index) => (
                <tr key={index}>
                    <td>{sale.salesperson.first_name}-{sale.salesperson.last_name}</td>
                    <td>{sale.customer.first_name}-{sale.customer.last_name}</td>
                    <td>{sale.automobile.vin}</td>
                    <td>{sale.price}</td>
                </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
    );
}

export default SalesHistoryList;
