import React, { useState, useEffect } from 'react';


function VehicleModelPage({ name, onDelete, picture_url, manufacturer }) {
    const handleDelete = () => {
    const confirmDelete = window.confirm('Are you sure you want to delete this vehicleModel?');
    if (confirmDelete) {
        onDelete();
    }
    };

    return (
    <tr>
        <td>{name}</td>
        <td>{manufacturer}</td>
        <td>
        <img
            src={picture_url}
            alt="model picture"
            style={{ maxWidth: '100px', maxHeight: '100px' }}
        />
        </td>
        <td>
        <button onClick={handleDelete}type="button" className="btn btn-danger">Delete</button>
        </td>
    </tr>
    );
}

function VehicleModelPageContainer() {
    const [vehicleModelsData, setVehicleModelsData] = useState([]);

    useEffect(() => {
    const fetchVehicleModelData = async () => {
        const url = 'http://localhost:8100/api/models/';
        try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('');
        } else {
            const data = await response.json();
            const vehicleModels = data.models.map(async (vehicleModel) => {
            const detailUrl = `http://localhost:8100/api/models/${vehicleModel.id}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                return {
                id: vehicleModel.id,
                name: details.name,
                picture_url: details.picture_url,
                manufacturer: details.manufacturer,
                };
            } else {
                throw new Error('Cant show models');
            }
            });

            Promise.all(vehicleModels)
            .then((vehicleModelDetails) => setVehicleModelsData(vehicleModelDetails))
            .catch((error) => console.error(error));
        }
        } catch (e) {
        console.error(e);
        alert('Error was raised!');
        }
    };

    fetchVehicleModelData();
    }, []);

    const handleDeleteVehicleModel = async (vehicleModelIndex) => {
    const vehicleModelToDelete = vehicleModelsData[vehicleModelIndex];
    const deleteUrl = `http://localhost:8100/api/models/${vehicleModelToDelete.id}`;
    try {
        const response = await fetch(deleteUrl, {
        method: 'DELETE',
        });

        if (response.ok) {
        setVehicleModelsData((prevVehicleModels) =>
            prevVehicleModels.filter((_, index) => index !== vehicleModelIndex)
        );
        } else {
        throw new Error('Error occurred while deleting the vehicleModel!');
        }
    } catch (e) {
        console.error(e);
        alert('Error occurred while deleting the vehicleModel!');
    }
    };

    return (
    <div>
        <h1>Model List</h1>
        <table id="vehicleModelContainer" className="table table-striped">
        <thead>
            <tr>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Picture</th>
            <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            {vehicleModelsData.map((vehicleModel, index) => (
            <VehicleModelPage
                key={index}
                name={vehicleModel.name}
                picture_url={vehicleModel.picture_url}
                manufacturer={vehicleModel.manufacturer.name}
                onDelete={() => handleDeleteVehicleModel(index)}
            />
            ))}
        </tbody>
        </table>
    </div>
    );
}

export default VehicleModelPageContainer;
