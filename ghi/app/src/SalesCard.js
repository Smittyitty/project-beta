import React from 'react';


function SaleCard(props) {
    if (props.sale === undefined) {
        return null;
    }
    const cardStyle = {
        backgroundColor: '#198754',
        padding: '10px',
        borderRadius: '8px',
        color: 'white'
      };
    return (
        <div className="col">
        <div key={props.sale.id} className="card mb-3 shadow">
        <div className="card-body" style={cardStyle}>
            <h6 className="card-title">Salesperson: {props.sale.salesperson.first_name} {props.sale.salesperson.last_name}</h6>
            <h6 className="card-subtitle mb-2">
                employee_id: {props.sale.salesperson.employee_id}
            </h6>
            <h6 className="card-subtitle mb-2">
                Customer: {props.sale.customer.first_name}{props.sale.customer.last_name}
            </h6>
            <h6 className="card-subtitle mb-2">
                VIN: {props.sale.automobile.vin}
            </h6>
            <h6 className="card-subtitle mb-2">
                Price: {props.sale.price}
            </h6>
        </div>
        </div>
    </div>
    );
}

export default SaleCard;
