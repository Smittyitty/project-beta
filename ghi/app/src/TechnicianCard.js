import React from 'react';


function TechnicianCard(props) {
  if (props.technician === undefined) {
    return null;
  }

  const handleDelete = () => {
    props.deleteTechnician(props.index, props.technician.employee_id);
  };
  const cardStyle = {
    backgroundColor: '#198754',
    padding: '10px',
    borderRadius: '10px 10px 0px 0px',
    color: 'white',
    alignItems: 'center'
  };
  return (
    <div className="col">
      <div key={props.technician.first_name} className="card mb-3 shadow">
        <div className="card-body" style={cardStyle}>
          <h5 className="card-title">First name: {props.technician.first_name}</h5>
          <h6 className="card-subtitle mb-2">
            Last name: {props.technician.last_name}
          </h6>
          <h6 className="card-subtitle mb-2">
            Employee ID: {props.technician.employee_id}
          </h6>
        </div>
        <div className="card-footer" style={{backgroundColor: 'black', display: 'flex', justifyContent: 'center', borderColor: "#198754", borderRadius: '0px 0px 10px 10px'}}>
          <button onClick={handleDelete} type="button" className="btn btn-danger">
            Delete
          </button>
        </div>
      </div>
    </div>
  );
}

export default TechnicianCard;
