import React, { useState, useEffect } from 'react';


function AppointmentForm() {
    const [dateName, setDate] = useState('');
    const [reasonName, setReason] = useState('');
    const [vinName, setVin] = useState('');
    const [customerName, setCustomer] = useState('');
    const [technicianName, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
      const fetchTechnicians = async () => {
        const techniciansURL = 'http://localhost:8080/api/technicians/';

        try {
          const response = await fetch(techniciansURL);
          if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
          } else {
            console.error('Failed to fetch technicians data:', response.status);
          }
        } catch (error) {
          console.error('Failed to fetch technicians data:', error);
        }
      };

      fetchTechnicians();
    }, []);

    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {
        date_time: dateName,
        reason: reasonName,
        vin: vinName,
        customer: customerName,
        technician: technicianName,
      };

      const appointmentURL = 'http://localhost:8080/api/appointments/';
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      try {
        const response = await fetch(appointmentURL, fetchConfig);
        if (response.ok) {
          const addAppointment = await response.json();

          setDate('');
          setReason('');
          setVin('');
          setCustomer('');
          setTechnician('');
          setTechnicians([])

          window.location.reload();
        } else {
          console.error('Failed to create appointment:', response.status);
        }
      } catch (error) {
        console.error('Failed to create appointment:', error);
      }
    };

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);

    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);

    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);

    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);

    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);

    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a service appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input onChange={handleDateChange} value={dateName} placeholder="Date Time" required type="datetime-local" name="date_time" id="date_time" className="form-control"/>
                <label htmlFor="date_time">Date/Time</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleReasonChange} value={reasonName} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vinName} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCustomerChange} value={customerName} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control"/>
                <label htmlFor="customer">Customer</label>
              </div>
              <div className="mb-3">
              <label htmlFor="technician"></label>
              <select onChange={handleTechnicianChange} name="technician" id="technician" className="form-select" value={technicianName || ''}>
                <option key="" value="">Choose a technician</option>
                {technicians.map((technician) => (
                  <option key={technician.employee_id} value={technician.employee_id}>
                    {technician.first_name} {technician.last_name}
                  </option>
                ))}
              </select>
            </div>
              <div className="mb-3">
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

  }
export default AppointmentForm;
