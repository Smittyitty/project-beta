import React, {useEffect, useState} from 'react';


function SalespersonForm() {
    const [firstName, setFirst] = useState('');
    const [lastName, setLast] = useState('');
    const [employeeidName, setEmployeeid] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeidName,
        }


        const salespersonURL = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(salespersonURL, fetchConfig);
        if (response.ok){
            const newSalesperson = await response.json();

            setFirst('');
            setLast('');
            setEmployeeid('');
        }
    }


    const handleFirstChange = (event) => {
        const value = event.target.value;
        setFirst(value)
    }

    const handleLastChange = (event) => {
        const value = event.target.value;
        setLast(value)
    }

    const handleEmployeeidChange = (event) => {
        const value = event.target.value;
        setEmployeeid(value)
    }


    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setEmployeeid(data.employee_id)
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salespeople-form">
              <div className="form-floating mb-3">
                <input onChange={handleFirstChange} value={firstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control"/>
                <label htmlFor="first_name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleLastChange} value={lastName} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control"/>
                <label htmlFor="last_name">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmployeeidChange} placeholder="employee id" required type="text" name="employee_name" id="employee_id" className="form-control"/>
                <label htmlFor="employee_id">employee Id</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

}

export default SalespersonForm;
