import React, {useEffect, useState } from 'react';


function AppointmentsList() {
    const [appointments, setAppointments] = useState([]);
    const [vin, setVin] = useState('');

    async function loadAppointments(vin) {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok && (vin === '' || vin === undefined) ) {
        const data = await response.json();
        setAppointments(data.appointments);
    } else if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments.filter((appointment) => appointment.vin === String(vin)));
    }
    }

    useEffect(() => {
    loadAppointments();
    }, []);

    const handleChangeVin = (event) => {
    const value = event.target.value;
    setVin(value);
    }
    
    function search() {
    loadAppointments(vin);
    }

    return (
        <div className="container my-4">
        <h1 className="display-5 fw-bold">Service History</h1>
        <form className="d-flex my-4" onSubmit={e => e.preventDefault()}>
            <input onChange={handleChangeVin} className="form-control" type="search" placeholder="Search by VIN..." aria-label="Search"/>
            <button className="btn btn-primary" onClick={() => search()}>Search</button>
        </form>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
    {appointments && appointments.map((appointment, index) => {
    let vip_status = "No";
    if (appointment.vip) { vip_status = "Yes" };
    return (
    <tr key={index}>
        <td>{appointment.vin}</td>
        <td>{vip_status}</td>
        <td>{appointment.customer}</td>
        <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
        <td>{new Date(appointment.date_time).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}</td>
        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
        <td>{appointment.reason}</td>
        <td>{appointment.status}</td>
    </tr>
    );
    })}
            </tbody>
        </table>
        </div>
    );
    }

    export default AppointmentsList;
