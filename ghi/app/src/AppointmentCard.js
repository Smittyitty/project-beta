import React from 'react';


function AppointmentCard(props) {
  if (props.appointment === undefined) {
    return null;
  }

  const handleCancel = () => {
    props.updateAppointmentStatus(props.appointment.id, 'canceled');
    props.deleteAppointment(props.index, props.appointment.vin);
  };

  const handleFinished = () => {
    props.updateAppointmentStatus(props.appointment.id, 'finished');
    props.deleteAppointment(props.index, props.appointment.vin);
  };

  const options = {
    timeZone: 'UTC',
  };

  const appointmentTime = new Date(props.appointment.date_time);
  const formattedTime = appointmentTime.toLocaleTimeString('en-US', options);
  const appointmentDate = new Date(props.appointment.date_time);

  const cardStyle = {
    backgroundColor: '#198754',
    padding: '10px',
    borderRadius: '10px 10px 0px 0px',
    color: 'white'
  };
  return (
    <div className="col">
      <div key={props.appointment.date_time} className="card mb-3 shadow">
        <div className="card-body" style={cardStyle}>
          <h5 className="card-title">Reason: {props.appointment.reason}</h5>
          <h6 className="card-subtitle mb-2">Status: {props.appointment.status}</h6>
          <h6 className="card-subtitle mb-2">Date: {appointmentDate.toLocaleDateString()}</h6>
          <p>Time: {formattedTime}</p>
          <h6 className="card-subtitle mb-2">VIN: {props.appointment.vin}</h6>
          <h6 className="card-subtitle mb-2">Customer: {props.appointment.customer}</h6>
          <h6 className="card-subtitle mb-2">Technician: {props.appointment.technician.first_name} {props.appointment.technician.last_name}</h6>
        </div>
        <div className="card-footer" style={{backgroundColor: 'black', display: 'flex', justifyContent: 'center', borderColor: "#198754", borderRadius: '0px 0px 10px 10px'}}>
          <button onClick={handleCancel} type="button" className="btn btn-danger">
            Cancel
          </button>
          <button onClick={handleFinished} type="button" className="btn btn-success">
            Finish
          </button>
        </div>
      </div>
    </div>
  );
}

export default AppointmentCard;
