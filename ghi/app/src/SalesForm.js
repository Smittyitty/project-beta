import React, { useState, useEffect } from 'react';


function SaleForm() {
    const [automobileName, setAutomobile] = useState('');
    const [salespersonName, setSalesperson] = useState('');
    const [customerName, setCustomer] = useState('');
    const [priceName, setPrice] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([])
    const [automobiles, setAutomobiles] = useState([])

    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {
        automobile: automobileName,
        salesperson: salespersonName,
        customer: customerName,
        price: priceName,
      };

      const saleURL = 'http://localhost:8090/api/sales/';
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

          const response = await fetch(saleURL, fetchConfig);
          if (response.ok) {
            const addsale = await response.json();
            event.target.reset()
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
            setSalespeople([]);
            setCustomers([]);
            setAutomobiles([]);
          }
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);

    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);

    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const curl = 'http://localhost:8090/api/customers/';
        const vurl = 'http://localhost:8100/api/automobiles/'

        const salespeopleResponse = await fetch(url);
        const customersResponse = await fetch(curl);
        const automobilesResponse = await fetch(vurl);
        if (customersResponse.ok && salespeopleResponse.ok && automobilesResponse.ok) {
            const salespeopledata = await salespeopleResponse.json();
            const customersdata = await customersResponse.json();
            const automobilesdata = await automobilesResponse.json();

            setSalespeople(salespeopledata.salespeople);
            setCustomers(customersdata.customers);
            setAutomobiles(automobilesdata.autos);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Sale</h1>
              <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="mb-3">
                  <select
                    onChange={handleSalespersonChange}
                    value={salespersonName}
                    required
                    name="salesperson"
                    id="salesperson"
                    className="form-select"
                  >
                  <option value="">Choose a Salesperson</option>
                  {salespeople.map((salesperson) => (
                  <option value={salesperson.employee_id} key={salesperson.employee_id}>
                    First name: {salesperson.first_name} Last Name: {salesperson.last_name} employee id: {salesperson.employee_id}
                  </option>
                ))}
              </select>
              </div>
              <div className="mb-3">
                  <select
                    onChange={handleCustomerChange}
                    value={customerName}
                    required
                    name="customer"
                    id="customer"
                    className="form-select"
                  >
                <option value="">Choose a Customer</option>
                {customers.map((customer) => (
                  <option value={customer.id} key={customer.id}>
                    First name: {customer.first_name} Last Name: {customer.last_name} id: {customer.id}
                  </option>
                ))}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={handleAutomobileChange}
                  value={automobileName || ''}
                  required
                  name="automobile"
                  id="automobile"
                  className="form-select"
                >
                <option value="">Choose an automobile</option>
                {automobiles.map((automobile) => (
                  <option value={automobile.vin} key={automobile.vin}>
                    {automobile.vin}
                  </option>
                ))}
              </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={priceName} placeholder="price" required type="text" name="price" id="price" className="form-control"/>
                <label htmlFor="price">Price</label>
              </div>
              <div className="mb-3">
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
    }
export default SaleForm;
