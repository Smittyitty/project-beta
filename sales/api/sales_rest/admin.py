from django.contrib import admin
from .models import Salesperson, Sale, AutomobileVO, Customer
# Register your models here.


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    pass


@admin.register(AutomobileVO)
class AutomobileVoAdmin(admin.ModelAdmin):
    pass


@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    pass


@admin.register(Customer)
class CustomerVoAdmin(admin.ModelAdmin):
    pass
