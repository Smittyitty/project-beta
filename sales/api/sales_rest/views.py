from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .encoders import CustomerListEncoder, SalespersonListEncoder, SaleEncoder
from .models import Salesperson, Sale, Customer, AutomobileVO


@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder=SalespersonListEncoder
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message:": "SALESPERSON doesnt Exist"},
                status=400
            )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonListEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson NOT CREATED"},
                status=400
            )


@require_http_methods(["DELETE"])
def delete_salesperson(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Salesperson.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0}
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message:": "SALESPERSON doesnt Exist"},
                status=400
            )


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerListEncoder,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {
                    "message:": "customer doesn't exist"
                },
                status=400
            )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer NOT CREATED"},
                status=400
            )


@require_http_methods(["DELETE"])
def delete_customer(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Customer.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0}
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message:": "Customer doesnt Exist"},
                status=400
            )


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": list(sales)},
            encoder=SaleEncoder,
            safe=False
        )
    else:
        if request.method == "POST":
            content = json.loads(request.body)
            try:
                vin = content["automobile"]
                automobile = AutomobileVO.objects.get(vin=vin)
                content["automobile"] = automobile
                # if automobile.sold:
                #     return JsonResponse(
                #         {"message": "Sorry, but this automobile has been sold already, explore other options of your liking."}
                #     )
                # else:
                #     content["automobile"] = automobile
                #     automobile.sold = True
                #     automobile.save()
            except AutomobileVO.DoesNotExist:
                return JsonResponse(
                    {"message": "No Valid vin number"},
                    status=400,
                )
            try:
                salesperson_id = content["salesperson"]
                salesperson = Salesperson.objects.get(id=salesperson_id)
                content["salesperson"] = salesperson
            except Salesperson.DoesNotExist:
                return JsonResponse(
                    {"message": "No Valid salesperson id"},
                    status=400,
                )
            try:
                customer_id = content["customer"]
                customer = Customer.objects.get(id=customer_id)
                content["customer"] = customer
            except Customer.DoesNotExist:
                return JsonResponse(
                    {"message": "No Valid customer id"},
                    status=400,
                )
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )


@require_http_methods(["DELETE"])
def delete_sale(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Sale.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0}
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message:": "SALE doesnt Exist"},
                status=400
            )
