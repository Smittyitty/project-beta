from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)
    

    def __str__(self):
        return f"{self.vin}, sold:{self.sold}"
    

class Technician(models.Model):
    first_name = models.CharField(max_length= 200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=100, unique=True)


    def __str__(self):
        return self.first_name
    

    def get_api_url(self):
        return reverse("api_list_technicians", kwargs={"id": self.employee_id})

    
class Appointment(models.Model):
    date_time = models.DateTimeField(null=True)
    reason = models.TextField(blank=True)
    status = models.CharField(max_length=20, default="Scheduled")
    vin = models.CharField(max_length=200, unique=True)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name ="appointment",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return f"Appointment: {self.customer} - {self.date_time}"
    
    def finish(self):
        status = "FINISHED"
        self.status = status
        self.save()

    def cancel(self):
        status = "canceled"
        self.status = status
        self.save()
